#ifndef AUTemplate_Parameters_h
#define AUTemplate_Parameters_h


// rename all three strings as needed.
#define kBundleName CFSTR("com.algorhythmaudio.ProjName")
#define kViewName CFSTR("AUTemplateView")
#define kViewClass CFSTR("AUTemplateViewFactory")

#define kVERSION 0x000001
#define kHasCustomView 0 // make 1 for yes and 0 for no


// List all params here.  They can be generic names like kParam_1 or more specific.
enum {
    kParam_GainL,
    kParam_GainR,
    kParamNum // dont touch this
};


// this is an example of a simple parameter used as a gain knob.
static CFStringRef  kParam1Name     = CFSTR("Gain Left");
const float         kParam1Default  = 0.5;
const float         kParam1Min      = 0.0;
const float         kParam1Max      = 1.0;

static CFStringRef  kParam2Name     = CFSTR("Gain Right");
const float         kParam2Default  = 0.5;
const float         kParam2Min      = 0.0;
const float         kParam2Max      = 1.0;









#endif /* defined(__AUTemplate_Parameters_h) */











