#include "AUTemplate.h"




#pragma mark Create and Destroy

/* this is the constructor method.  this is where all the init happens */

AUTemplatePluginKernal :: AUTemplatePluginKernal(AUEffectBase *inAudioUnit) : AUKernelBase(inAudioUnit) {
    puts("kernal constructor");
    sr = GetSampleRate();
}


// make sure to dealloc anything malloced here
AUTemplatePluginKernal::~AUTemplatePluginKernal() {
    
}


// here you define all the parameters that will be used
AUTemplatePlugin::AUTemplatePlugin(AudioUnit component) : AUEffectBase(component, false) {
    
    CreateElements();
    Globals() -> UseIndexedParameters(kParamNum);
    
    // set defaults here
    SetParameter(kParam_GainL, kParam1Default);
    SetParameter(kParam_GainR, kParam2Default);
    
    // this sets the factory preset, no need to change this
    SetAFactoryPresetAsCurrent(kPresets[kPresetDefault]);
}


// this can update the view if uninitialized (see FilterDemo)
OSStatus AUTemplatePlugin::Initialize() {
    OSStatus result = AUEffectBase::Initialize();
    
    if (result == noErr) {
        puts("initialized");
        // here you add copy protection code
    }
    
    return result;
}


void AUTemplatePluginKernal::Reset() {
    // shouldn't need to reset anything here
}




#pragma mark - Getters - Parameters

// this function sets different parts for each parameter
OSStatus AUTemplatePlugin::GetParameterInfo(AudioUnitScope			inScope,
                                          AudioUnitParameterID      inParameterID,
                                          AudioUnitParameterInfo	&outParameterInfo )
{
    ComponentResult result = noErr;
    
    // tells the host that all the parameters are readable and writable
    outParameterInfo.flags = kAudioUnitParameterFlag_IsReadable | kAudioUnitParameterFlag_IsWritable;
    
    if (inScope == kAudioUnitScope_Global) {
        switch (inParameterID) {
            case kParam_GainL:
                AUBase::FillInParameterName(outParameterInfo, kParam1Name, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_LinearGain;
                outParameterInfo.minValue = kParam1Min;
                outParameterInfo.maxValue = kParam1Max;
                outParameterInfo.defaultValue = kParam1Default;
                break;
            case kParam_GainR:
                AUBase::FillInParameterName(outParameterInfo, kParam2Name, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_LinearGain;
                outParameterInfo.minValue = kParam2Min;
                outParameterInfo.maxValue = kParam2Max;
                outParameterInfo.defaultValue = kParam2Default;
                break;
            default:
                result = kAudioUnitErr_InvalidParameter;
                break;
        }
    }
    else
        result = kAudioUnitErr_InvalidParameter;
    
    return result;
}


/* This method is so the au can get the names of each of the indexed values only for kAudioUnitParameterUnit_Indexed.
   See RingMod for an example of an indexed type.
 */
OSStatus AUTemplatePlugin::GetParameterValueStrings(AudioUnitScope		inScope,
                                                  AudioUnitParameterID	inParameterID,
                                                  CFArrayRef *			outStrings)
{
    
    return  kAudioUnitErr_InvalidProperty;
}




#pragma mark Properties


// no need to touch this
OSStatus AUTemplatePlugin::GetPropertyInfo(AudioUnitPropertyID    inID,
                                         AudioUnitScope			inScope,
                                         AudioUnitElement		inElement,
                                         UInt32                 &outDataSize,
                                         Boolean                &outWritable )
{
    if (inScope == kAudioUnitScope_Global) {
        switch (inID) {
            case kAudioUnitProperty_CocoaUI:
                if (kHasCustomView) {
                    puts("GetPropertyInfo call and ready for cocoa view");
                    outWritable = false;
                    outDataSize = sizeof(AudioUnitCocoaViewInfo);
                    return noErr;
                }
        }
    }
    
    return AUEffectBase::GetPropertyInfo(inID, inScope, inElement, outDataSize, outWritable);
}


// no need to touch this
OSStatus AUTemplatePlugin::GetProperty(AudioUnitPropertyID 	inID,
                                     AudioUnitScope 		inScope,
                                     AudioUnitElement 		inElement,
                                     void 					*outData )
{
    if (inScope == kAudioUnitScope_Global) {
        switch (inID) {
            case kAudioUnitProperty_CocoaUI: // allows host to find the custom view
                if (kHasCustomView) {
                    CFBundleRef bundle = CFBundleGetBundleWithIdentifier(kBundleName);
                    if (bundle == NULL) return fnfErr;
                    
                    CFURLRef bundleURL = CFBundleCopyResourceURL(bundle, kViewName, CFSTR("bundle"), NULL);
                    if (bundleURL == NULL) return fnfErr;
                    
                    CFStringRef className = kViewClass;
                    AudioUnitCocoaViewInfo cocoaInfo = {bundleURL, className};
                    *((AudioUnitCocoaViewInfo*) outData) = cocoaInfo;
                    
                    return noErr;
                }
        }
    }
    
    // if we've gotten this far, handles the standard properties
	return AUEffectBase::GetProperty (inID, inScope, inElement, outData);
}




#pragma mark Presets

// this function prepares the presets.  No need to touch this
OSStatus AUTemplatePlugin::GetPresets (CFArrayRef *outData) const {
    if (outData == NULL) return noErr;
    
    CFMutableArrayRef presetsArray = CFArrayCreateMutable(NULL, kPresetNum, NULL);
    
    for (int i=0; i<kPresetNum; i++)
        CFArrayAppendValue(presetsArray, &kPresets[i]);
    
    *outData = (CFArrayRef) presetsArray;
    return noErr;
}







#pragma mark - Utility


// define presets here.
OSStatus AUTemplatePlugin::NewFactoryPresetSet (const AUPreset &inNewFactoryPreset) {
    SInt32 choosenPreset = inNewFactoryPreset.presetNumber;
    
    // must include all possible presets
    if (kPreset1 || kPreset2 || kPreset3) {
        for (int i=0; i<kPresetNum; i++) {
            if (choosenPreset == kPresets[i].presetNumber) {
                switch (choosenPreset) {
                    case kPreset1:
                        SetParameter(kParam_GainL, kParam1Default);
                        SetParameter(kParam_GainR, kParam2Default);
                        break;
                    case kPreset2:
                        SetParameter(kParam_GainL, 1.0);
                        SetParameter(kParam_GainR, 0.0);
                        break;
                    case kPreset3:
                        SetParameter(kParam_GainL, 0.0);
                        SetParameter(kParam_GainR, 1.0);
                        break;
                }
                
                SetAFactoryPresetAsCurrent(kPresets[i]);
                return noErr;
            }
        }
    }
    
    return kAudioUnitErr_InvalidProperty;
}


// shouldn't need to touch this
void AUTemplatePlugin::SetMaxFramesPerSlice(UInt32 nFrames) {
    AUBase::SetMaxFramesPerSlice(nFrames);
}




#pragma mark - Private







#pragma mark - Process

/********************************************************************************************
                                       Processing
 
 @Usage
    - First decide if your plugin needs channel interaction.  If so, you can get rid of the
      override of ProcessBufferLists() and then override Process().  This will deal with all
      the buffer hassle.  You can also remove all the other functions below.  Make sure to
      also remove them from the header file.
    - If you want stereo interaction, then it's a bit more confusing.  First ProcessBufferLists()
      will look at the number of outputs for the audio unit (which is selected by the host).
      It will call one of three proc functions accordingly: ProcessMono(), ProcessStereo(), and
      ProcessOther().
 
      @Process
 
        - ProcessMono() is for a single output and fairly simple to code.  It calls dspMono() so you
          can leave that alone for now.  Within dspMono() you can use the code provided, and just
          modify the dsp loop as needed.
    
        - ProcessStereo() is where it gets complicated.  It simply checks if the buffers are 
          interleaved or not.  It calls dspStereoInterleaved() or dspStereoDeinterleaved() 
          accordingly.  You can leave this funtion alone.
        
        @Stream Type
 
            - dspStereoInterleaved() just add the user parameters, and then add your dsp code
              for the left and the right.  The current implementation doesn't have channel 
              interaction, but each channel is processed differently.  Make sure to leave the
              line of code that says "interleave it again".  Note that this code has not been
              tested yet.
 
            - dspSTereoDeInterleaved() is more straight forward.  You can basically just add
              your dsp code under the sections "DSP left" and "DSP right".  Each channel is
              a seperate buffer making it easier to maintain order.
 
        - ProcessOther() is for more than 2 output channels.  This is for surround tracks and
          it's possible to just have it use one, or both, of the other dsp functions for brevity.
 
    @Summery
        - If you want N to N implementation, then delete all the functions below, and just override
          the Process() function.  If you want channel interaction, or channel specific code, then
          add your dsp to the three dsp functions below.
 
 
 @Notes:
     - In the case of input channels < output channels, the audio unit duplicates input frames so
       input frames == output frames == output channels.  This hasn't been varified, but is 
       currently assumed, and the code is implemented accordingly.
     - Basic implementation has stereo channel interaction, but more than 2 output channels only
       has N=N interaction.
     - Interleaved hasn't been tested.  This is only for AU version 1, which I don't think I have
       a host that uses it.
 
 
 
*********************************************************************************************/

OSStatus AUTemplatePlugin::ProcessBufferLists(AudioUnitRenderActionFlags    &ioActionFlags,
                                              const AudioBufferList         &inBuffer,
                                              AudioBufferList               &outBuffer,
                                              UInt32                        inFramesToProcess)
{
    // first check for silence and bypass if true
	bool silentInput = IsInputSilent (ioActionFlags, inFramesToProcess);
	ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;
    
    if (silentInput)
        return noErr;
    else
        ioActionFlags &= ~kAudioUnitRenderAction_OutputIsSilence;
    
    // find output channels, and process accordingly.
    switch (GetNumberOfChannels()) {
        case 1:
            this->ProcessMono(ioActionFlags, inBuffer, outBuffer, inFramesToProcess);
            break;
        case 2:
            this->ProcessStereo(ioActionFlags, inBuffer, outBuffer, inFramesToProcess);
            break;
        default:
            this->ProcessOther(ioActionFlags, inBuffer, outBuffer, inFramesToProcess);
            break;
    }
    
    return noErr;
}


// mono.  Other process methods may call this one for processing with no channel relation
void AUTemplatePlugin :: ProcessMono(AudioUnitRenderActionFlags &ioActionFlags,
                                     const AudioBufferList      &inBuffer,
                                     AudioBufferList            &outBuffer,
                                     UInt32						inFramesToProcess)
{
    //puts("mono in mono out");
    // mono input, mono output, no hassle
    this->dspMono((const Float32*) inBuffer.mBuffers[0].mData,
                  (Float32*) outBuffer.mBuffers[0].mData,
                  inFramesToProcess);
}


// stereo output
void AUTemplatePlugin :: ProcessStereo(AudioUnitRenderActionFlags   &ioActionFlags,
                              const AudioBufferList                 &inBuffer,
                              AudioBufferList                       &outBuffer,
                              UInt32                                inFramesToProcess)
{
    // check for interleaved
    if (inBuffer.mNumberBuffers == 1) {
        //puts("interleaved");
        this->dspStereoInterleaved(inBuffer, outBuffer, inFramesToProcess);
    }
    // deinterleaved, with stereo interaction
    else {
        //puts("deinterleaved");
        this->dspStereoDeinterleaved(inBuffer, outBuffer, inFramesToProcess);
    }
}


// unknown num of output channels
void AUTemplatePlugin :: ProcessOther(AudioUnitRenderActionFlags    &ioActionFlags,
                             const AudioBufferList                  &inBuffer,
                             AudioBufferList                        &outBuffer,
                             UInt32                                 inFramesToProcess)
{
    // check for interleaved
    if (inBuffer.mNumberBuffers == 1) {
        for (int i=0; i<GetNumberOfChannels(); i++) {
            this->dspMono((const Float32*) inBuffer.mBuffers[0].mData + i,
                          (Float32*) outBuffer.mBuffers[0].mData + i,
                          inFramesToProcess);
        }
    }
    // deinterleaved, but no stereo interaction
    else {
        for (int i=0; i < 2; i++) {
            this->dspMono((const Float32*) inBuffer.mBuffers[i].mData,
                          (Float32*) outBuffer.mBuffers[i].mData,
                          inFramesToProcess);
        }
    }
}


void AUTemplatePlugin :: dspMono(const Float32                      *inSourceP,
                                 Float32                            *inDestP,
                                 UInt32								inFramesToProcess)

{
    // first build local buffers
    const Float32 *input = inSourceP;
    Float32 *output = inDestP;
    
    // get parameters
    Float32 gain = GetParameter(kParam_GainL);
    
    // DSP
    for (int i=0; i<inFramesToProcess; i++) {
        output[i] = input[i] * gain;
    }
}


void AUTemplatePlugin :: dspStereoInterleaved(const AudioBufferList &inBuffer,
                                              AudioBufferList       &outBuffer,
                                              UInt32                inFramesToProcess)
{
    UInt32 channels = GetNumberOfChannels();
    
    Float32 *input[channels];
    Float32 *output[channels];
    
    for (int i=0; i<channels; i++) {
        input[i] = (Float32*) inBuffer.mBuffers[0].mData + i;
        output[i] = (Float32*) outBuffer.mBuffers[0].mData +i;
    }
    
    // get parameters
    Float32 gainL = GetParameter(kParam_GainL);
    Float32 gainR = GetParameter(kParam_GainR);
    
    // DSP
    for (int i=0; i<channels; i++) {
        for (int j=0; j<inFramesToProcess; j++) {
            // left
            if (i%2 == 0)
                output[i][j] = input[i][j] * gainL;
            // right
            else
                output[i][j] = input[i][j] * gainR;
            
            output[i][j] += i; // interleave it again
        }
    }
    
    
    
}


void AUTemplatePlugin :: dspStereoDeinterleaved(const AudioBufferList   &inBuffer,
                                                AudioBufferList         &outBuffer,
                                                UInt32                  inFramesToProcess)
{
    Float32 *outBuffL;
    Float32 *outBuffR;
    
    // get parameters
    Float32 gainL = GetParameter(kParam_GainL);
    Float32 gainR = GetParameter(kParam_GainR);
    
    // DSP
    for (int i=0; i < GetNumberOfChannels(); i++) {
        const AudioBuffer *srcBuffer = &inBuffer.mBuffers[i];
        AudioBuffer *destBuffer = &outBuffer.mBuffers[i];

        // left
        if (i%2 == 0) {
            const Float32 *inBuffL = (const Float32*) srcBuffer->mData;
            outBuffL = (Float32*) destBuffer->mData;

            // DSP Left
            for (int j=0; j<inFramesToProcess; j++)
                outBuffL[j] = inBuffL[j] * gainL;
        }
        // right
        else {
            const Float32 *inBuffR = (const Float32*) srcBuffer->mData;
            outBuffR = (Float32*) destBuffer->mData;
            
            // DSP Right
            for (int j=0; j<inFramesToProcess; j++)
                outBuffR[j] = inBuffR[j] * gainR;
        }
    }
}













