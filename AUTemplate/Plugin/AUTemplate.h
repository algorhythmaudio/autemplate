#ifndef __AUTemplate__AUTemplate__
#define __AUTemplate__AUTemplate__


#include <AudioToolbox/AudioUnitUtilities.h>
#include "AUEffectBase.h"



// This is in ms and for any effect that continues to process after all audio
// packets have been received.
#define kAllowedTailTime (Float64) 0.0 // default is 0






class AUTemplatePluginKernal : public AUKernelBase {
    
    
    // private ivars and functions can go here that are specific to DSP
private:
    
    Float64         sr;
    
public:
    
    AUTemplatePluginKernal (AUEffectBase *inAudioUnit);
    virtual ~AUTemplatePluginKernal ();
    virtual void    Reset();
    
};


class AUTemplatePlugin : public AUEffectBase {
    
    
public:
    AUTemplatePlugin (AudioUnit component);
    virtual AUKernelBase *NewKernel() {return new AUTemplatePluginKernal(this);}
    
    virtual OSStatus Version() {return kVERSION;}
    virtual OSStatus Initialize();
    
    // parameter
    virtual OSStatus GetParameterInfo           (AudioUnitScope			inScope,
                                                 AudioUnitParameterID   inParameterID,
                                                 AudioUnitParameterInfo	&outParameterInfo );
    virtual OSStatus GetParameterValueStrings   (AudioUnitScope         inScope,
                                                 AudioUnitParameterID	inParameterID,
                                                 CFArrayRef *			outStrings);
    
    // property
    virtual OSStatus GetPropertyInfo            (AudioUnitPropertyID    inID,
                                                 AudioUnitScope			inScope,
                                                 AudioUnitElement		inElement,
                                                 UInt32                 &outDataSize,
                                                 Boolean                &outWritable );
    
    virtual OSStatus GetProperty                (AudioUnitPropertyID 	inID,
                                                 AudioUnitScope 		inScope,
                                                 AudioUnitElement 		inElement,
                                                 void 					*outData );
    
    
    virtual OSStatus GetPresets                 (CFArrayRef *outData) const;
    virtual OSStatus NewFactoryPresetSet        (const AUPreset &inNewFactoryPreset);
    
    virtual void SetMaxFramesPerSlice           (UInt32 nFrames);
    virtual	bool SupportsTail () { return true; } // this should always be true for effects
    virtual Float64 GetTailTime() {return kAllowedTailTime;}
    
    
    // This is the dsp engine
    virtual OSStatus ProcessBufferLists(AudioUnitRenderActionFlags &ioActionFlags,
                                        const AudioBufferList &inBuffer,
                                        AudioBufferList &outBuffer,
                                        UInt32 inFramesToProcess);
    
private:
    
    // proc functions depending on number of output channels
    void            ProcessMono(AudioUnitRenderActionFlags &	ioActionFlags,
                                const AudioBufferList &			inBuffer,
                                AudioBufferList &				outBuffer,
                                UInt32							inFramesToProcess);
    
    void            ProcessStereo(AudioUnitRenderActionFlags &	ioActionFlags,
                                  const AudioBufferList &			inBuffer,
                                  AudioBufferList &				outBuffer,
                                  UInt32							inFramesToProcess);
    
    void            ProcessOther(AudioUnitRenderActionFlags &	ioActionFlags,
                                 const AudioBufferList &			inBuffer,
                                 AudioBufferList &				outBuffer,
                                 UInt32							inFramesToProcess);
    
    // independent channel processing
    void            dspMono(const Float32                       *inSourceP,
                            Float32                             *inDestP,
                            UInt32								inFramesToProcess);

    // channel interaction processing
    void            dspStereoInterleaved(const AudioBufferList &inBuffer,
                                         AudioBufferList       &outBuffer,
                                         UInt32                inFramesToProcess);
    
    void            dspStereoDeinterleaved(const AudioBufferList &inBuffer,
                                           AudioBufferList       &outBuffer,
                                           UInt32                inFramesToProcess);




    
    
};




AUDIOCOMPONENT_ENTRY(AUBaseFactory, AUTemplatePlugin);





// Presets
enum {
    kPreset1 = 0,
    kPreset2,
    kPreset3,
    kPresetNum
};

static AUPreset kPresets [kPresetNum] = {
    {kPreset1, CFSTR("Preset 1 Name")},
    {kPreset2, CFSTR("Preset 2 Name")},
    {kPreset3, CFSTR("Preset 3 Name")}
};

// factory default
static const int kPresetDefault = kPreset1;
















#endif /* defined(__AUTemplate__AUTemplate__) */
