#include <AudioUnit/AudioUnit.r>

#define RES_ID          1000
#define COMP_TYPE       'aufx'
#define COMP_SUBTYPE    'DEVL'
#define COMP_MANUF      'Algo'
#define VERSION         0x000001
#define NAME            "AlgoRhythm Audio: AU Template"
#define DESCRIPTION     "An audio unit template"
#define ENTRY_POINT     "AUTemplatePluginEntry"

#include "AUResources.r"