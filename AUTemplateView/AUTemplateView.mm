#import "AUTemplateView.h"


// add each new parameter to this list. Same as under Parameters.h
AudioUnitParameter parameterList[] = {  {0, kParam_GainL, kAudioUnitScope_Global, 0},
                                        {0, kParam_GainR, kAudioUnitScope_Global, 0}};



@implementation AUTemplateView

-(id) initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        // load images from the bundle to NSImage's
        //NSBundle *bundle = [NSBundle bundleWithIdentifier:(NSString*) kBundleName];
        //background = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMBackground" ofType:@"png"]];
        
        // set mouse rects
        //rectSine = NSMakeRect(11, 86, 68, 28);
        
        mouseClick = -1; // -1 means no selection
        [self setNeedsDisplay:YES];
    }
    return self;
}



#pragma mark - Drawing


// used for rotating knobs
-(NSImage *) rotateImage:(NSImage *)image angle:(int)alpha {
    NSImage *existingImage = image;
    NSSize existingSize = [existingImage size];
    NSSize newSize = NSMakeSize(existingSize.height, existingSize.width);
    NSImage *rotatedImage = [[NSImage alloc] initWithSize:newSize];
    
    [rotatedImage lockFocus];
    [[NSGraphicsContext currentContext]
     setImageInterpolation:NSImageInterpolationNone];
    
    NSAffineTransform *rotateTF = [NSAffineTransform transform];
    NSPoint centerPoint = NSMakePoint(newSize.width / 2,
                                      newSize.height / 2);
    
    //translate the image to bring the rotation point to the center
    //(default is 0,0 ie lower left corner)
    
    [rotateTF translateXBy:centerPoint.x yBy:centerPoint.y];
    [rotateTF rotateByDegrees:alpha];
    [rotateTF translateXBy:-centerPoint.x yBy:-centerPoint.y];
    [rotateTF concat];
    
    [existingImage drawAtPoint:NSZeroPoint
                      fromRect:NSMakeRect(0, 0, newSize.width, newSize.height)
                     operation:NSCompositeSourceOver
                      fraction:1.0];
    
    [rotatedImage unlockFocus];
    
    return rotatedImage;
}


-(void) drawRect:(NSRect)dirtyRect {
    [NSGraphicsContext saveGraphicsState];
    
    // draw background
    
    
    
    [NSGraphicsContext restoreGraphicsState];
}


#pragma mark - Audio


// this is the method that the AU calls to send parameter data to the view
// add each parameter to the switch and update the views that interact with it.
- (void)_parameterListener: (void*)                     inObject
                 parameter: (const AudioUnitParameter*) inParameter
                     value: (AudioUnitParameterValue)   inValue
{
    // inObject ignored in this case.

	switch (inParameter->mParameterID) {
		case kParam_GainL:
            
            break;
        case kParam_GainR:
            
            break;
    }
}


void ParameterListenerDispatcher (void *inRefCon, void *inObject, const AudioUnitParameter *inParameter, AudioUnitParameterValue inValue) {
	AUTemplateView *SELF = (AUTemplateView *)inRefCon;
    
    [SELF _parameterListener:inObject parameter:inParameter value:inValue];
}


- (void)_addListeners {
	verify_noerr (AUListenerCreate (ParameterListenerDispatcher,
                                    self,
                                    CFRunLoopGetCurrent(),
                                    kCFRunLoopDefaultMode,
                                    0.100, // 100ms standard. This is min time allowed between calls
                                    &mParamListener));
	
    for (int i = 0; i < kParamNum; i++) {
        parameterList[i].mAudioUnit = myAU;
        verify_noerr (AUListenerAddParameter(mParamListener, NULL, &parameterList[i]));
    }
}


- (void)_removeListeners {
    for (int i = 0; i < kParamNum; i++)
        verify_noerr (AUListenerRemoveParameter(mParamListener, NULL, &parameterList[i]) );
    
	verify_noerr (AUListenerDispose(mParamListener) );
}


- (void)_synchronizeUIWithParameterValues {
    AudioUnitParameterValue value;
    
    for (int i=0; i<kParamNum; i++) {
        // first get the parameter id
        verify_noerr(AudioUnitGetParameter(myAU,
                                           parameterList[i].mParameterID,
                                           kAudioUnitScope_Global,
                                           0,
                                           &value));
        
        // this sets the parameter and notifies the listener
        verify_noerr (AUParameterSet (mParamListener,
                                      self,
                                      &parameterList[i],
                                      value,
                                      0));
        
        // this will notify the view of changes in the code (eg: presets)
        verify_noerr (AUParameterListenerNotify(mParamListener,
                                                self,
                                                &parameterList[i]));
    }
    
}




#pragma mark - Mouse


-(void) mouseDown:(NSEvent *)theEvent {
    //NSPoint mouseLocation = [theEvent locationInWindow];
    //NSPoint convertedLocation = [self convertPoint:mouseLocation fromView:nil];
    
    
    // check for mouse hit within a rect, if so set mouseClick to some value
}


-(void) mouseDragged:(NSEvent *)theEvent {
    if (mouseClick != 101 && mouseClick != 102) return;
    
    //NSPoint mouseLocation = [theEvent locationInWindow];
    //NSPoint convertedLocation = [self convertPoint:mouseLocation fromView:nil];
    
    
    switch (mouseClick) {
        case 101:
            
            break;
        case 102:
            
            break;
    }
    
    
    [self setNeedsDisplay:YES];
}


-(void) mouseUp:(NSEvent *)theEvent {
    if (mouseClick == -1) return;
    
    //NSPoint mouseLocation = [theEvent locationInWindow];
    //NSPoint convertedLocation = [self convertPoint:mouseLocation fromView:nil];
    
    // -1 means no selection
    mouseClick = -1;
    [self setNeedsDisplay:YES];
}









#pragma mark - Public


-(void) setAU: (AudioUnit) inAU {
    NSLog(@"setAU called");
    if (myAU) [self _removeListeners];
    
    myAU = inAU;
    
    if (myAU) {
        NSLog(@"inAU passed");
        [self _addListeners];
        [self _synchronizeUIWithParameterValues];
    }
    else
        NSLog(@"couldn't sync parameters with view");
}


#pragma mark View Setters



//-(void) setValueFreq:(double) freq {
//    if (freq < kParam1Min) freq = kParam1Min;
//    if (freq > kParam1Max) freq = kParam1Max;
//
//    valueFreq = freq;
//
//    [textFreq setStringValue:[NSString stringWithFormat:@"%.2f", freq]];
//    [labelFreq setStringValue:[NSString stringWithFormat:@"%.2f", freq]];
//
//    // other code to update AU
//    verify_noerr(AUParameterSet(mParamListener,
//                                NULL,
//                                &parameterList[0], // kParam1 is frequency
//                                (Float32) freq,
//                                0));
//}








#pragma mark IBAction












@end
























