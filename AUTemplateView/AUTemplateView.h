#import <Cocoa/Cocoa.h>
#import <AudioUnit/AudioUnit.h>
#import <AudioToolbox/AudioToolbox.h>




@interface AUTemplateView : NSView {
    /*~~~~~~~~~~~~~~~~ mouseClick ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * - keeps track of where the user clicked on the interface
     * - value of -1 means no click
     * - value of kParam3Type's for the waveType
     * - value of 101 for knobFreq, 102 for knobDepth
     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    int                     mouseClick;
    CGFloat                 dragStart;
    CGFloat                 dragDialPos;
    
    
    
@protected
    // other members
    AudioUnit               myAU;
    AUParameterListenerRef  mParamListener;
}


-(void) setAU: (AudioUnit) inAU;








@end


























