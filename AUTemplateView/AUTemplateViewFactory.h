#import <Cocoa/Cocoa.h>
#import <AudioUnit/AUCocoaUIView.h>



@class AUTemplateView;


@interface AUTemplateViewFactory : NSObject <AUCocoaUIBase> {
    IBOutlet AUTemplateView *uiView;
}

- (NSString *) description;	// string description of the view

@end